import gym
from gym import error, spaces, utils
from gym.spaces import Box
from gym.utils import seeding
import numpy as np
import time
import vtk
import threading
import itertools
import copy

from gym_uav.envs.utils import TimerCallback
from gym_uav.envs.utils import Config_MultiAgent
from gym_uav.envs.utils import Smoother_soft


class UavMultiEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, num_agents):
        print("using normal environment with sparse reward, the environment version is uav-v0")
        Common = Config_MultiAgent()
        self.num_agents = num_agents
        self.basic_directions = Common.basic_directions
        self.extra_directions = Common.extra_directions
        self.original_observation_length = Common.original_observation_length
        self.gap = Common.gap
        self.init_gap = Common.initial_gap
        self.exist_agent = np.zeros([self.num_agents, 2])
        self.extra_length = len(self.extra_directions)

        self.observation_space = Box(-np.inf, np.inf, [self.original_observation_length + self.extra_length], float)
        self.action_space = Box(-1.0, 1.0, [2], float)
        self._env_step_counter = 0
        self.reward_type = "dense"

        self.state = np.zeros([self.num_agents, self.observation_space.shape[0]])  # 定义无人机状态向量并随机初始化

        # 无人机参数
        self.level = Common.level
        self.position = np.zeros([self.num_agents, 2])
        self.target = np.zeros([2])
        self.orient = np.zeros([self.num_agents, 1])
        self.speed = np.zeros([self.num_agents, 1])  # 定义无人机的初始速度为0.0
        self.max_speed = Common.max_speed
        self.min_distance_to_target = Common.min_distance_to_target
        self.real_action_range = Common.real_action_range

        # self.File = open('./path_agent.txt', 'w+')

        # 环境参数
        self.min_distance_to_obstacle = Common.min_distance_to_obstacle
        self.min_initial_starts = Common.min_initial_starts
        self.expand = Common.expand
        self.num_circle = Common.num_circle
        self.radius = Common.radius
        self.period = Common.period
        self.mat_height = None  # 环境中建筑物的高度
        self.mat_exist = None  # 行高度范围内的建筑
        self.lowest = Common.lowest
        self.delta = Common.delta
        self.total = Common.total

        # range finder parameters
        self.scope = Common.scope
        self.min_step = Common.min_step
        self.directions = self.basic_directions + self.extra_directions
        self.end_points = [[None for _ in range(len(self.directions))] for _ in range(self.num_agents)]

        # rendering parameters
        self.margin = Common.margin
        self.env_params = {'cylinders': None, 'size': 1.5*(self.num_circle+self.margin*2)*self.period,
                           'departure': None, 'arrival': None}
        self.agent_params = [{'position': self.position[j, :], 'target': self.target, 'direction':None,
                              'rangefinders': self.end_points[j], 'is_init':True} for j in range(self.num_agents)]
        self.agent_params_pre = None
        self.first_render = True
        self.terminate_render = False
        self.camera_alpha = Common.camera_alpha

        # other parameters
        self.is_reset = False

        assert self.scope > self.max_speed
        assert self.reward_type == "dense" or self.reward_type == "sparse"

    def _fast_range_finder(self, position, theta, forward_dist, min_dist=0.0, find_type='normal'):
        # print("steps and forward", forward_dist, self.min_step, forward_dist)
        end_cache = copy.deepcopy(position)
        position_integer = np.floor(end_cache / self.period).astype(np.int)
        judge = end_cache - (position_integer * self.period + self.period / 2)
        # print("end_cache", end_cache, "position_integer", position_integer, judge)
        if judge[0] >= 0 and judge[1] > 0:
            # print("in one")
            down_left = position_integer * self.period + self.period / 2
            down_right = (position_integer + np.array([1, 0])) * self.period + self.period / 2
            up_left = (position_integer + np.array([0, 1])) * self.period + self.period / 2
            up_right = (position_integer + np.array([1, 1])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] + 1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] + self.expand, position_integer[1] + 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + 1 + self.expand, position_integer[1] + 1 + self.expand]]))
        elif judge[0] >= 0 and judge[1] < 0:
            # print('in two')
            down_left = (position_integer + np.array([0, -1])) * self.period + self.period / 2
            down_right = (position_integer + np.array([1, -1])) * self.period + self.period / 2
            up_left = position_integer * self.period + self.period / 2
            up_right = (position_integer + np.array([1, 0])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[position_integer[0] + 1 + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + 1 + self.expand, position_integer[1] + self.expand]]))
        elif judge[0] < 0 and judge[1] > 0:
            # print("in three")
            down_left = (position_integer + np.array([-1, 0])) * self.period + self.period / 2
            down_right = position_integer * self.period + self.period / 2
            up_left = (position_integer + np.array([-1, 1])) * self.period + self.period / 2
            up_right = (position_integer + np.array([0, 1])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] -1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] - 1 + self.expand, position_integer[1] + 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + 1 + self.expand]]))
        else:
            # print("in four")
            down_left = (position_integer + np.array([-1, -1])) * self.period + self.period / 2
            down_right = (position_integer + np.array([0, -1])) * self.period + self.period / 2
            up_left = (position_integer + np.array([-1, 0])) * self.period + self.period / 2
            up_right = position_integer * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] - 1 + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] - 1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + self.expand]]))

        base_points = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'], [down_left, down_right, up_left, up_right]))

        # print(base_points, end_cache)
        dist = []
        end = []
        for base in base_points.keys():
            theta_base = np.arctan(np.abs((base_points[base] - end_cache)[0] / (base_points[base] - end_cache)[1]))
            if base == 'down_left':
                theta_base = np.pi + theta_base
            if base == 'down_right':
                theta_base = np.pi - theta_base
            if base == 'up_left':
                theta_base = 2 * np.pi - theta_base
            if base == 'up_right':
                theta_base = theta_base

            theta_base = np.mod(theta_base, 2*np.pi)

            dist_to_base = np.linalg.norm(end_cache - base_points[base])

            delta_theta = theta - theta_base
            if dist_to_base - (self.radius + min_dist) >= forward_dist or exists[base] < 0:
                dist.append(1.0)
                end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                # print("pass I")
            elif (dist_to_base - (self.radius + min_dist) >= 0) and (np.cos(delta_theta) <= 0):
                dist.append(1.0)
                end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                # print("pass II")
            else:
                # print("in III")
                min_dist_to_origin = np.abs(np.sin(delta_theta)) * dist_to_base

                if min_dist_to_origin >= (self.radius + min_dist):
                    dist.append(1.0)
                    end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                else:
                    # print(base, 'min_dist_to_origin', min_dist_to_origin)

                    dist_inner = np.sqrt((self.radius + min_dist) ** 2 - min_dist_to_origin ** 2)
                    # theta_inner = np.arccos(min_dist_to_origin / (self.radius + min_dist))
                    # print(base, 'theta_inner', theta_inner / np.pi * 180)
                    # dist_inner = (self.radius + min_dist) * np.sin(theta_inner)
                    # print(base, 'dist_inner', dist_inner, 'dist_to_base', dist_to_base)

                    final_dist = np.cos(delta_theta) * dist_to_base - dist_inner
                    final_dist = final_dist[0]
                    # print(base, 'final_dist', final_dist)
                    if final_dist >= forward_dist:
                        dist.append(1.0)
                        end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                    else:
                        # print("in final", final_dist)
                        dist.append(final_dist / forward_dist)
                        end.append(end_cache + np.array([final_dist * np.sin(theta[0]), final_dist * np.cos(theta[0])]))
        dist = np.array(dist)

        return np.min(dist), end[np.argmin(dist)]

    def _range_finder(self, position, theta, steps, min_dist=0.0, find_type='normal'):
        end_cache = copy.deepcopy(position)
        Count = 0
        state = 1.0

        while Count < steps:
            Count = Count + 1
            end_cache = end_cache + np.array([self.min_step * np.sin(theta[0]), self.min_step * np.cos(theta[0])])
            end = np.mod(end_cache, self.period)

            position_integer = np.floor(end_cache / self.period).astype(np.int)
            if self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand] > 0:
                if np.linalg.norm(end - np.array([self.period / 2, self.period / 2])) - (self.radius + min_dist) <= 0:
                    state = np.linalg.norm(end_cache - position) / self.scope
                    break

        return state, end_cache

    def _prepare_background_for_render(self):
        small_mat_height = self.mat_height[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                           self.expand - self.margin: self.expand + self.num_circle + self.margin]
        small_mat_exist = self.mat_exist[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                          self.expand - self.margin: self.expand + self.num_circle + self.margin]
        index_tmp = [i - self.margin for i in range(np.shape(small_mat_height)[0])]
        position_tmp = list(itertools.product(index_tmp, index_tmp))
        position_tmp = [list(pos) for pos in position_tmp]
        position_tmp = np.array(position_tmp)
        position_tmp = position_tmp * self.period + self.period / 2

        cylinders = []
        small_mat_height = list(small_mat_height.reshape(1,-1)[0])
        small_mat_exist = list(small_mat_exist.reshape(1,-1)[0])
        position_tmp = list(position_tmp)

        for hei, exi, pos in zip(small_mat_height, small_mat_exist, position_tmp):
            if exi > 0:
                p1 = np.concatenate([pos, np.array([0])])
                p2 = np.concatenate([pos, np.array([hei])])
                r = self.radius
                cylinders.append([p1, p2, r])

        self.env_params['cylinders'] = copy.deepcopy(cylinders)
        print("check", np.shape(self.position), np.shape(np.expand_dims(np.array([self.level] * self.num_agents), 0)))
        self.env_params['departure'] = copy.deepcopy(np.concatenate((self.position,
                                                                     np.expand_dims(np.array([self.level] * self.num_agents), 1)), 1))

        self.env_params['arrival'] = copy.deepcopy(np.concatenate([self.target, np.array([self.level])]))
        # np.savetxt('./mat_height.txt', self.mat_height, fmt='%d', delimiter=' ', newline='\r\n')
        # np.savetxt('./mat_exist.txt', self.mat_exist, fmt='%d', delimiter=' ', newline='\r\n')

    def _get_observation(self, position, target, orient):
        # print("called observation")
        self.agent_params_pre = copy.deepcopy(self.agent_params)

        for j in range(self.num_agents):
            global_counter = 0
            basic_counter = 0
            extra_counter = 0
            for dir in self.basic_directions:
                theta = np.mod(dir + orient[j, :], 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度
                # print("bug check", position[j, :], theta, self.scope)
                self.state[j, basic_counter], end_cache = self._fast_range_finder(position[j, :], theta, self.scope)
                self.end_points[j][global_counter] = [np.concatenate([position[j, :], np.array([self.level])]),
                                                       np.concatenate([end_cache, np.array([self.level])])]

                global_counter += 1
                basic_counter += 1

            # adding extra range finders
            for dir in self.extra_directions:
                theta = np.mod(dir + orient[j, :], 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度
                # self.state[15 + extra_counter], end_cache = self._range_finder(position, theta, int(self.scope/self.min_step))
                self.state[15 + extra_counter], end_cache = self._fast_range_finder(position[j, :], theta, self.scope)
                self.end_points[j][global_counter] = [np.concatenate([position[j, :], np.array([self.level])]),
                                                       np.concatenate([end_cache, np.array([self.level])])]
                global_counter += 1
                extra_counter += 1

            dist = np.linalg.norm(target - position[j, :])
            # self.state[9] = 2 / (np.exp(-0.002 * dist) + 1) - 1
            self.state[j, 9] = 2*(dist / (np.sqrt(2)*self.period * self.num_circle) - 0.5)
            # 计算目标和当前位置的相对夹角
            theta_target = np.arctan((target[0] - position[j, 0]) / (target[1] - position[j, 1]))
            if (target[0] >= position[j, 0]) and (target[1] >= position[j, 1]):
                self.state[j, 10] = np.sin(theta_target)
                self.state[j, 11] = np.cos(theta_target)
            elif target[1] < position[j, 1]:
                self.state[j, 10] = np.sin(theta_target + np.pi)
                self.state[j, 11] = np.cos(theta_target + np.pi)
            else:
                self.state[j, 10] = np.sin(theta_target + 2 * np.pi)
                self.state[j, 11] = np.cos(theta_target + 2 * np.pi)

            # 保存目标的绝对方向角
            self.state[j, 12] = np.sin(orient[j, :])  # normalization
            self.state[j, 13] = np.cos(orient[j, :])  # normalization
            # 保存速度为另一个状态
            self.state[j, 14] = 2*(self.speed[j, :] / self.max_speed - 0.5)

            # get mutual states
            distance = np.linalg.norm(position[j, :] - position, axis=1)
            distance_sorted = np.sort(distance)
            counter_left = 0
            counter_right = 0
            for g in range(1, self.num_agents):
                agent_index_temp = np.argmax(1 - np.abs(np.sign(distance - distance_sorted[g])))
                theta_agent_temp = np.arctan((position[agent_index_temp, 0] - position[j, 0]) / (
                            0.00000001 + (position[agent_index_temp, 1] - position[j, 1])))
                if (position[agent_index_temp, 0] >= position[j, 0]) * (
                        position[agent_index_temp, 1] >= position[j, 1]):
                    theta_agent_temp = theta_agent_temp
                elif position[agent_index_temp, 1] < position[j, 1]:
                    theta_agent_temp = theta_agent_temp + np.pi
                else:
                    theta_agent_temp = theta_agent_temp + 2 * np.pi
                delta_theta = np.mod(theta_agent_temp - orient[j, 0], 2 * np.pi)
                if (delta_theta >= np.pi) * (counter_left == 0):
                    counter_left = 1
                    # print('distance_left', distance_sorted[g])
                    self.exist_agent[j, 0] = 1
                    self.state[j, 15] = np.sin(theta_agent_temp)
                    self.state[j, 16] = np.cos(theta_agent_temp)
                    self.state[j, 17] = 2 / (np.exp(-0.02 * distance_sorted[g]) + 1) - 1
                if (delta_theta < np.pi) * (counter_right == 0):
                    counter_right = 1
                    # print('distance_right', distance_sorted[g])
                    self.exist_agent[j, 1] = 1
                    self.state[j, 18] = np.sin(theta_agent_temp)
                    self.state[j, 19] = np.cos(theta_agent_temp)
                    self.state[j, 20] = 2 / (np.exp(-0.02 * distance_sorted[g]) + 1) - 1

                if (counter_left > 0) * (counter_right > 0):
                    break
            if counter_left == 0:
                # print('distance_left inf')
                self.exist_agent[j, 0] = 0
                self.state[j, 15] = np.sin(3.0 / 2.0 * np.pi)
                self.state[j, 16] = np.cos(3.0 / 2.0 * np.pi)
                self.state[j, 17] = 2 / (np.exp(-0.02 * 20.0) + 1) - 1
            if counter_right == 0:
                # print('distance_right inf')
                self.exist_agent[j, 1] = 0
                self.state[j, 18] = np.sin(1.0 / 2.0 * np.pi)
                self.state[j, 19] = np.sin(1.0 / 2.0 * np.pi)
                self.state[j, 20] = 2 / (np.exp(-0.02 * 20.0) + 1) - 1

            self.agent_params[j]['position'] = copy.deepcopy(np.concatenate([position[j, :], np.array([self.level])]))
            self.agent_params[j]['target'] = copy.deepcopy(np.concatenate([target, np.array([self.level])]))
            self.agent_params[j]['rangefinders'] = copy.deepcopy(self.end_points[j])
            self.agent_params[j]['direction'] = copy.deepcopy(np.mod(90 - orient[j, :]/2/np.pi*360, 360))
            self.agent_params[j]['direction_camera'] = copy.deepcopy(
                np.mod(90 - np.mod(self.orient_render[j, :], 2 * np.pi) / 2 / np.pi * 360, 360))
            if self._env_step_counter > 1:
                self.agent_params[j]['is_init'] = False
            else:
                self.agent_params[j]['is_init'] = True

    def step(self, action):
        assert self.is_reset, 'the environment must be reset before it is called'
        self._env_step_counter += 1
        position_temp = np.copy(self.position)
        self.orient = np.mod(self.real_action_range[0] * action[:, 0:1] * np.pi + self.orient, 2 * np.pi)

        self.orient_total_pre = copy.deepcopy(self.orient_total)
        self.orient_render_pre = copy.deepcopy(self.orient_render)
        self.orient_total = self.real_action_range[0] * action[:, 0:1] * np.pi + self.orient_total
        self.orient_render = self.orient_total * self.camera_alpha \
                             + self.orient_render * (1 - self.camera_alpha)

        self.speed = np.where(action[:, 1:] >= 0,
                              self.speed + self.real_action_range[1] * action[:, 1:] * (-np.tanh(0.5 * (self.speed - self.max_speed))),
                              self.speed + self.real_action_range[1] * action[:, 1:] * np.tanh(0.5 * self.speed))

        done1, end_cache = [], []
        # print("position check", self.position[0, :], self.orient[0, :], self.speed[0, 0])
        for i in range(self.num_agents):
            print("input check", self.position[i, :], self.orient[i, :], self.speed[i, 0])
            done1_tmp, end_cache_tmp = self._fast_range_finder(np.copy(self.position[i, :]), np.copy(self.orient[i, :]),
                                                       np.copy(self.speed[i, 0]), self.min_distance_to_obstacle, 'forward')
            done1.append(done1_tmp)
            end_cache.append(end_cache_tmp)

        done1 = np.array(done1)
        print("position and end cache", self.speed, self.position, np.array(end_cache))
        self.position = np.copy(end_cache)
        # print("don1 check", self.position, done1)

        self._get_observation(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        next_observation = np.copy(self.state)

        mutual_distance_left = np.log(2 / (self.state[:, 17] + 1) - 1) / (-0.02)
        mutual_distance_right = np.log(2 / (self.state[:, 20] + 1) - 1) / (-0.02)
        print("before done1", done1)
        done1 = (done1 < 1.0).any()
        # print("done1 results", done1)
        done2 = (np.linalg.norm(self.position - self.target, axis=1) <= self.min_distance_to_target).any()
        done3 = (np.linalg.norm(self.position - self.target, axis=1) >= 1e4).any()

        # terminal judgement ###########################################################################################
        done = done1 + done2 + done3
        if done1:
            print('agent collides with obstacles!')
        if done2:
            print('agent arrived at the destination!')
        if done3:
            print('agent is too far from the target position!')

        # Reward #######################################################################################################
        if self.reward_type == "sparse":
            reward_sparse = np.where(done2, np.zeros([1]) + 1.0, np.zeros([1]))
            reward = reward_sparse[0]
        else:
            reward_sparse = np.where(done2, np.zeros([self.num_agents]) + 15.0, np.zeros([self.num_agents]))
            reward_distance = np.tanh(0.2 * (10.0 - self.speed)) * (
                        np.linalg.norm(position_temp - self.target, axis=1) - np.linalg.norm(
                    self.position - self.target, axis=1))
            reward_barrier = np.where(np.min(self.state[:, 0:9], 1) * 100 <= 10.0, -5.0 + np.zeros([self.num_agents]),
                                      np.zeros([self.num_agents]))
            reward_action = -3.0
            reward_mutual = np.where((mutual_distance_left <= 50) * (mutual_distance_right <= 50),
                                     3.0 * np.exp(-np.square(mutual_distance_left - 20) * 0.05) + 3.0 * np.exp(
                                         -np.square(mutual_distance_right - 20) * 0.05), np.zeros([self.num_agents]))
            reward_agent = np.where(mutual_distance_left <= 10.0, np.zeros([self.num_agents]) - 5.0,
                                    np.zeros([self.num_agents])) \
                           + np.where(mutual_distance_right <= 10.0, np.zeros([self.num_agents]) - 5.0,
                                      np.zeros([self.num_agents]))

            reward = reward_sparse + reward_barrier + reward_distance + reward_action + reward_mutual + reward_agent

        info = {'reward_type': 'sparse'}
        if done2:
            info.update({'is_success': True})
        elif done1:
            info.update({'is_crash': True})
        elif done3:
            info.update({'is_termination': True})
        else:
            pass

        if done:
            self.terminate_render = True

        return next_observation, reward, done, info

    def reset(self):
        self.is_reset = True
        self.first_render = True
        self.terminate_render = False
        self._env_step_counter = 0

        self.mat_height = np.random.randint(
            1, 10, size=(self.num_circle + 2 * self.expand, self.num_circle + 2 * self.expand)) * self.delta + self.lowest  # 建筑物高度
        W, H = np.shape(self.mat_height)
        for i in range(W):
            for j in range(H):
                if self.mat_height[i, j] > self.level + self.delta:
                    self.mat_height[i, j] = self.mat_height[i, j] + np.int(np.random.uniform(0, 200))

        self.mat_exist = self.mat_height - self.level  # 确定飞行高度范围内的建筑
        while True:
            position = np.random.uniform(0, self.period, size=(self.num_agents, 2))
            if (np.linalg.norm(position - np.array([self.period / 2, self.period / 2]), axis=1)
                - (self.radius + self.min_distance_to_obstacle) > 0).all():
                break
        relative_position = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
        self.position = position + relative_position * self.period

        # ##############################其次产生目标位置#####################################

        while True:
            target = np.random.uniform(0, self.period, size=(2,))
            if np.linalg.norm(target - np.array([self.period / 2, self.period / 2])) - self.radius > 0:
                break

        counter = 0
        while True:  # ensure that the minimum distance between initial position and target position is larger than 200m
            counter += 1
            relative_target = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
            target_temp = np.array(target + relative_target * self.period)
            if (np.linalg.norm(target_temp - self.position, axis=1) >= self.min_initial_starts).all():
                self.target = target + relative_target * self.period
                #################################################################################################
                self.orient = np.random.uniform(0, 2 * np.pi, size=(self.num_agents, 1))
                self.orient_render = copy.deepcopy(self.orient)
                self.orient_total = copy.deepcopy(self.orient)
                self.orient_render_pre = copy.deepcopy(self.orient_render)
                self.orient_total_pre = copy.deepcopy(self.orient_total)
                self.speed = np.zeros([self.num_agents, 1])
                self._prepare_background_for_render()
                #################################################################################################
                # print("obsertvation check", np.shape(self.position), np.shape(self.target), np.shape(self.orient))
                self._get_observation(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
                observation = np.copy(self.state)
                break
            elif counter > 20:
                print("reset again")
                return self.reset()
            else:
                pass

        return observation

    def render(self, mode='human'):
        print("position in render", self.position)
        sleep_time = 0.2
        # print('orient={}, speed={}'.format(self.orient, self.speed))
        # self.File.write(str(self.target[0]))
        # self.File.write(' ')
        # self.File.write(str(self.target[1]))
        # self.File.write(' ')
        # self.File.write(str(self.position[0]))
        # self.File.write(' ')
        # self.File.write(str(self.position[1]))
        # self.File.write(' ')
        # for j in range(len(self.state)):
        #     self.File.write(str(self.state[j]))
        #     self.File.write(' ')
        # self.File.write(str(self.speed[0]))
        # self.File.write(' ')
        # self.File.write(str(self.orient[0]))
        # self.File.write('\n')

        assert self.is_reset, 'the environment must be reset before rendering'
        if self.first_render:
            time.sleep(sleep_time)
            self.first_render = False
            renderer = vtk.vtkRenderer()
            renderer.SetBackground(.2, .2, .2)
            # Render Window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.AddRenderer(renderer)
            renderWindow.SetSize(1600, 1600)
            self.Timer = TimerCallback(renderer)
            self.Timer.env_params = self.env_params

            def environment_render():
                renderWindowInteractor = vtk.vtkRenderWindowInteractor()
                renderWindowInteractor.SetRenderWindow(renderWindow)
                renderWindowInteractor.Initialize()
                renderWindowInteractor.AddObserver('TimerEvent', self.Timer.execute)
                timerId = renderWindowInteractor.CreateRepeatingTimer(30)
                self.Timer.timerId = timerId
                # renderWindow.Start()
                renderWindowInteractor.Start()

            self.th = threading.Thread(target=environment_render, args=())
            # print(threading.Event())
            # self.event = threading.Event()
            self.th.start()
            time.sleep(1.0)
        else:
            self.Timer.terminate_render = self.terminate_render
            positions_list, directions_list = [], []
            division = 5
            for i in range(self.num_agents):
                positions, directions, _ = \
                    Smoother_soft(self.agent_params_pre[i]['position'], self.agent_params[i]['position'],
                                  self.orient_total_pre[i, 0],
                                  self.orient_total[i, 0],
                                  self.orient_render_pre[i, 0],
                                  self.orient_render[i, 0], divisions=division)
                for i in range(len(directions)):
                    directions[i] = np.array([np.mod(90 - np.mod(directions[i] / 2 / np.pi * 360.0, 360), 360)])
                directions_list.append(directions)
                positions_list.append(positions)

            directions_list = np.array(directions_list)
            positions_list = np.array(positions_list)

            positions_render = np.mean(positions_list, 0)

            # print("position", np.shape(positions_list), positions_list, positions_render)

            orient_total_pre_mean = np.mean(self.orient_total_pre, 0)
            orient_total_mean = np.mean(self.orient_total, 0)
            orient_render_pre_mean = np.mean(self.orient_render_pre, 0)
            orient_render_mean = np.mean(self.orient_render, 0)

            _, _, directions_camera = Smoother_soft(None, None, orient_total_pre_mean, orient_total_mean,
                                                    orient_render_pre_mean, orient_render_mean, divisions=division)

            for i in range(len(directions_camera)):
                directions_camera[i] = np.array(
                    [np.mod(90 - np.mod(directions_camera[i] / 2 / np.pi * 360.0, 360), 360)])

            for j in range(division):
                for i in range(self.num_agents):
                    time.sleep(sleep_time / division / self.num_agents)
                    # print("agent={}, is init={}".format(i, self.agent_params[i]['is_init']))
                    # if self.agent_params[i]['is_init']:
                    #     time.sleep(0.1 / self.num_agents)
                    agent_params_tmp = copy.deepcopy(self.agent_params[i])
                    agent_params_tmp['position'] = positions_list[i, j]
                    agent_params_tmp['position_camera'] = positions_render[j]
                    agent_params_tmp['direction'] = directions_list[i, j, 0]
                    agent_params_tmp['direction_camera'] = directions_camera[j]
                    if j < division - 1:
                        agent_params_tmp['rangefinders'] = None
                    else:
                        agent_params_tmp['rangefinders'] = self.agent_params[i]['rangefinders']
                    self.Timer.agent_params = agent_params_tmp

            # if self.terminate_render:
            #     self.event.set()
            #
            #     print('threading is stopped', self.event.is_set())

    def seed(self, seed=None):
        if seed:
            if seed >= 0:
                np.random.seed(seed)
    #
    # def close(self):
    #     pass


if __name__ == '__main__':
    agents = 10
    env = UavMultiEnv(agents)
    env.reset()
    for i in range(1000):
        action = np.array([env.action_space.sample() for _ in range(agents)])
        # action[:, 0] = 0.25 * action[:, 0]
        obe, rew, done, info = env.step(action)
        env.render()
        if done:
            exit(0)
            env.reset()


